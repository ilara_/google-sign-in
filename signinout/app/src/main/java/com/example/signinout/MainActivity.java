package com.example.signinout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {

    GoogleSignInClient mGoogleSignInClient;
    Button logout;
    SignInButton signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        logout = findViewById(R.id.button_sign_out);
        signin = findViewById(R.id.sign_in_button);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        updateUI(account);
    }

    private void updateUI(final GoogleSignInAccount account) {
        if (account != null) {
            signin.setVisibility(View.GONE);
            logout.setVisibility(View.VISIBLE);

            TextView nama = findViewById(R.id.user);
            nama.setText( account.getDisplayName()+ " | " +account.getEmail() );

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signOut();
                }
            });
        } else {
            signin.setVisibility(View.VISIBLE);
            logout.setVisibility(View.GONE);

            TextView txt = findViewById(R.id.user);
            txt.setText("HAH?! KOSONG?");

            Toast.makeText(MainActivity.this,
                    "SELAMAT DATANG GAN!!", Toast.LENGTH_LONG).show();

            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signIn();
                }
            });
        }
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        revokeAccess();
                    }
                });
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                        TextView txt = findViewById(R.id.user);
                        txt.setText("HAH?! KOSONG?");
                    }
                });
    }

    private void signIn() {
        Toast.makeText(MainActivity.this,
                " log in gan", Toast.LENGTH_LONG).show();

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 998);
//        Toast.makeText(MainActivity.this,
//                " udah gan", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Toast.makeText(MainActivity.this,
//                " 1", Toast.LENGTH_LONG).show();
         if (requestCode == 998) {
//            Toast.makeText(MainActivity.this,
//                    " 3", Toast.LENGTH_LONG).show();
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            Toast.makeText(MainActivity.this,
                  "udah nih gan", Toast.LENGTH_LONG).show();

          updateUI(account);
        } catch (ApiException e) {
//            Toast.makeText(MainActivity.this,
//                    " 2", Toast.LENGTH_LONG).show();
           updateUI(null);
        }
    }
}
